///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"

#include "minirel.h"

#define NUMBUF 20
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
// Hash Table size
//You should define the necessary classes and data structures for the hash table,
// and the queues for LSR, MRU, etc.

/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  {
    ERROR_OF_HASH, ERROR_OF_LIST, ERROR_OF_PIN
};

struct Hash {
	struct Bucket {
		PageId pageid;
		int frameid;
		int love_hate;
		void *page_node;
		struct Bucket *next;
	};
	Bucket *buckets[HTSIZE];

	int hashnumber(PageId pageid) {
	    return ((61 * pageid) + 37) % HTSIZE;
	}

	void start() {
		for (int i = 0; i < HTSIZE; ++i)
			buckets[i] = NULL;
	}

	void clean() {
		Bucket *bucket;
		for (int i = 0; i < HTSIZE; ++i) {
			while (buckets[i] != NULL) {
				bucket = buckets[i];
				buckets[i] = bucket->next;
				free(bucket);
			}
			buckets[i] = NULL;
		}
	}

	int search_bucket(PageId pageid, int *q, void **page_node) {
		int position = hashnumber(pageid);
		Bucket *head = buckets[position];
		while (head != NULL) {
			if (head->pageid == pageid) {
				*q = head->love_hate;
				*page_node = head->page_node;
				return head->frameid;
			}
			head = head->next;
		}
		return -1;
	}

	Status insert_page(PageId pageid, int frameid, int *bit, void **page_node) {
		int position = hashnumber(pageid);

		Bucket *new_head = (Bucket *)malloc(sizeof(Bucket));
		Bucket *old_head = NULL;
		if (buckets[position] != NULL)
			old_head = buckets[position];
		new_head->pageid = pageid;
		new_head->frameid = frameid;
		new_head->love_hate = *bit;
		new_head->page_node = *page_node;
		new_head->next = old_head;
		buckets[position] = new_head;
	    return OK;
	}

	Status remove_page(PageId pageid) {
		int position = hashnumber(pageid);
		Bucket *head = buckets[position];
		Bucket *next_node = head;
		if (head == NULL)
			return FAIL;
		if (head->pageid == pageid) {
			next_node = head->next;
			free(head);
			buckets[position] = next_node;
			return OK;
		}
		else {
			next_node = head->next;
			while (next_node != NULL) {
				if (next_node->pageid == pageid) {
					head->next = next_node->next;
					free(next_node);
					return OK;
				}
				head = next_node;
				next_node = next_node->next;
			}
			return FAIL;
		}
	}
};

struct Buffer_list {
	struct Love_node {
		PageId pageid;
		int frameid;
		struct Love_node *next;
		struct Love_node *prev;
	};
	struct Hate_node {
		PageId pageid;
		int frameid;
		struct Hate_node *next;
		struct Hate_node *prev;
	};
	struct Love_node *love_list;
	struct Hate_node *hate_list;

	void start() {
		love_list = NULL;
		hate_list = NULL;
	}

	void clean() {
		struct Hate_node *hate_node;
		struct Hate_node *hate_start;
		hate_start = hate_list;
		if (hate_start)
			hate_list = hate_list->next;
		while (hate_list != hate_start) {
			hate_node = hate_list;
			hate_list = hate_list->next;
			free(hate_node);
		}
		free(hate_start);
		hate_list = NULL;

		struct Love_node *love_node;
		struct Love_node *love_start;
		love_start = love_list;
		if (love_start)
			love_list = love_list->next;
		while (love_list != love_start)
		{
			love_node = love_list;
			love_list = love_list->next;
			free(love_node);
		}
		free(love_start);
		love_list = NULL;
	}

	Status victim_frame(int *result) {
		int victim;
		if (hate_list != NULL) {
			victim = hate_list->frameid;
			struct Hate_node *hate_node = hate_list;

			hate_list = hate_node->next;
			if (hate_list == hate_node)
				hate_list = NULL;
			if (hate_list) {
				hate_node->prev->next = hate_list;
				hate_list->prev = hate_node->prev;
			}

			free(hate_node);
			*result = victim;
		}
		else {
			if (love_list == NULL)
				return FAIL;
			struct Love_node *love_node = love_list->prev;
			victim = love_node->frameid;

			if (love_node == love_list) {
				free(love_node);
				love_list = NULL;
				*result = victim;
				return OK;
			}
            love_node->prev->next = love_node->next;
			love_node->next->prev = love_node->prev;
			free(love_node);
			*result = victim;
		}
        return OK;
	}

	Status insert_page(int bit, PageId pageid, int frameid, void **page_node) {
		if (bit == 0) {
			struct Hate_node *hate_node;
			hate_node = (struct Hate_node *)malloc(sizeof(struct Hate_node));

			hate_node->pageid = pageid;
    	    hate_node->frameid = frameid;

			if (hate_list == NULL)
			{
				hate_list = hate_node;
				hate_list->prev = hate_list;
				hate_list->next = hate_list;
				*page_node = hate_list;
				return OK;
			}

        	hate_node->next = hate_list;
    	    hate_node->prev = hate_list->prev;
	        hate_list->prev->next = hate_node;
            hate_list->prev = hate_node;
    	    hate_list = hate_node;
			*page_node = hate_list;
		}
		else {
			struct Love_node *love_node;
			love_node = (struct Love_node *)malloc(sizeof(struct Love_node));
            love_node->pageid = pageid;
            love_node->frameid = frameid;
            if (love_list == NULL) {
                love_list = love_node;
                love_list->prev = love_list;
                love_list->next = love_list;
				*page_node = love_list;
                return OK;
            }
            love_node->next = love_list;
            love_node->prev = love_list->prev;
            love_list->prev->next = love_node;
			love_list->prev = love_node;
            love_list = love_node;
			*page_node = love_list;
		}
        return OK;
	}

	Status remove_page(int bit, void **page_node) {
		if (bit == 0) {
			struct Hate_node *hate_node;
			hate_node = (struct Hate_node *)(*page_node);
            if (hate_node == NULL)
                return FAIL;
			if (hate_list == hate_node)
				hate_list = NULL;
            if (hate_node->prev->next == hate_node) {
                free(hate_node);
                return OK;
            }
            hate_node->prev->next = hate_node->next;
            hate_node->next->prev = hate_node;
            free(hate_node);
		}
		else {
			struct Love_node *love_node;
			love_node = (struct Love_node *)(*page_node);
            if (love_node == NULL)
                return FAIL;
            if (love_list == love_node)
                love_list = NULL;
            if (love_node->prev->next == love_node) {
                free(love_node);
                return OK;
            }
            love_node->prev->next = love_node->next;
            love_node->next->prev = love_node;
            free(love_node);
		}
        return OK;
	}
};

class Replacer;

class BufMgr {

private: // fill in this area
	int buffer;
	struct Hash hashtable;
	struct Buffer_list buffer_list;
	struct Descriptor {
		PageId pageid;
		int pin_count;
		bool dirty;
	};
	Descriptor* descriptor;

public:

    Page* bufPool; // The actual buffer pool

    BufMgr (int numbuf, Replacer *replacer = 0);
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk

    Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1);
        // call DB object to allocate a run of new pages and
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate
        // all these pages and return error

    Status freePage(PageId globalPageId);
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page

    Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */
    Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
    }
};

#endif
